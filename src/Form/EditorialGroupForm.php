<?php

namespace Drupal\editorial_group\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the editorial group entity edit forms.
 */
class EditorialGroupForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink($entity->label(), 'edit-form')->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'), 'edit-form')->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New editorial group %label has been created.', $message_arguments));
        $this->logger('editorial_group')->notice('Created new editorial group %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The editorial group %label has been updated.', $message_arguments));
        $this->logger('editorial_group')->notice('Updated editorial group %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.editorial_group.collection');

    return $result;
  }

}
