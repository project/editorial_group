<?php

namespace Drupal\editorial_group\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Editorial group settings for this site.
 */
class EnabledTypesForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The expirable key value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected $keyValueExpirable;

  /**
   * Constructs a PathautoSettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Defines the configuration object factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Manages entity type plugin definitions.
   * @param \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface $key_value_expirable
   *   The key value expirable factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, KeyValueStoreExpirableInterface $key_value_expirable) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->keyValueExpirable = $key_value_expirable;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('keyvalue.expirable')->get('editorial_type_list')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'editorial_group_enabled_types';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['editorial_group.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['enabled_entity_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Entity types'),
      '#description' => $this->t('Enable editorial group for the selected entity types.'),
      '#options' => $this->getCandidateEntityTypes(),
      '#default_value' => $this->config('editorial_group.settings')->get('enabled_entity_types'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $original_set = $this->config('editorial_group.settings')->get('enabled_entity_types') ?: [];
    $new_set = $types['new_set'] = array_values(array_filter($form_state->getValue('enabled_entity_types')));
    $types['create'] = array_filter($new_set, function ($group) use ($original_set) {
      return array_search($group, $original_set) === FALSE;
    });
    $types['remove'] = array_filter($original_set, function ($group) use ($new_set) {
      return array_search($group, $new_set) === FALSE;
    });

    if (empty($types['create']) && empty($types['remove'])) {
      // Nothing to do. Just reload the form.
      return;
    }

    // Redirect to the confirmation form.
    $this->keyValueExpirable->setWithExpire($this->currentUser()->id(), $types, 60);
    $form_state->setRedirect('editorial_group.settings.enabled_types_confirm');
  }

  /**
   * Get the entity types where editorial group can be enabled.
   *
   * @return array
   *   Array of entity types names keyed by their IDs.
   */
  protected function getCandidateEntityTypes() {
    $candidate_entity_types = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type) {
      if ($entity_type instanceof ContentEntityTypeInterface
        && $entity_type->getLinkTemplate('canonical')
        && $entity_type->getLinkTemplate('edit-form')) {
        $candidate_entity_types[$entity_type->id()] = $entity_type->getLabel();
      }
    }

    return $candidate_entity_types;
  }

}
