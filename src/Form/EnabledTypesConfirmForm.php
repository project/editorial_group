<?php

namespace Drupal\editorial_group\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Confirmation before enable or disable editorial groups on entity types.
 */
class EnabledTypesConfirmForm extends ConfirmFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The expirable key value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected $keyValueExpirable;

  /**
   * Array of entity type IDs to enable or disable from editorial group.
   *
   * @var array
   */
  protected $types = [];

  /**
   * Constructs a PathautoSettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Defines the configuration object factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Manages entity type plugin definitions.
   * @param \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface $key_value_expirable
   *   The key value expirable factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, KeyValueStoreExpirableInterface $key_value_expirable) {
    $this->setConfigFactory($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->keyValueExpirable = $key_value_expirable;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('keyvalue.expirable')->get('editorial_type_list')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'editorial_group_enabled_types_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to do this?');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Continue');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Would you like to continue with the above?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('editorial_group.settings.enabled_types');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $account = $this->currentUser()->id();
    $this->types = $this->keyValueExpirable->get($account);

    // Redirect to the modules list page if the key value store is empty.
    if (!$this->types) {
      return $this->redirect('editorial_group.settings.enabled_types');
    }

    $form['message'] = $this->buildActionsMessage($form_state);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the new set into configuration.
    $this->configFactory->getEditable('editorial_group.settings')
      ->set('enabled_entity_types', $this->types['new_set'])
      ->save();

    $form_state->setRedirectUrl(new Url('editorial_group.settings.enabled_types'));
  }

  /**
   * Builds a message with the list of types to be enabled or disabled.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Render array with types to enable or disable.
   */
  protected function buildActionsMessage(FormStateInterface $form_state) {
    $build = [];
    if (!empty($this->types['create'])) {
      $build['create'] = [
        '#type' => 'container',
        'title' => [
          '#type' => 'html_tag',
          '#tag' => 'h2',
          '#value' => $this->t('Entity types to be enabled'),
          '#weight' => -1,
        ],
        'list' => [
          '#theme' => 'item_list',
          '#items' => $this->buildItems($this->types['create']),
        ],
        'description' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t('Base field <em>Editorial group</em> will be created on these entity types. It can be disabled per bundle just hidding this field in the display form settings.'),
          '#weight' => 99,
        ],
      ];
    }

    if (!empty($this->types['remove'])) {
      $build['remove'] = [
        '#type' => 'container',
        'title' => [
          '#type' => 'html_tag',
          '#tag' => 'h2',
          '#value' => $this->t('Entity types to be disabled'),
          '#weight' => -1,
        ],
        'list' => [
          '#theme' => 'item_list',
          '#items' => $this->buildItems($this->types['remove']),
        ],
        'description' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t('Editorial group field will be removed from these entity types. If you continue, <em>the currently existing values will be lost</em>. This operation cannot be undone.'),
          '#weight' => 99,
        ],
      ];
    }

    return $build;
  }

  /**
   * Builds a list of entity types IDs and their names.
   *
   * @param array $type_ids
   *   Type IDs.
   *
   * @return array
   *   Array of entity type names keyed by their IDs.
   */
  protected function buildItems(array $type_ids) {
    $list = [];
    foreach ($type_ids as $type_id) {
      $list[$type_id] = $this->entityTypeManager->getDefinition($type_id)->getLabel();
    }

    return $list;
  }

}
