<?php

namespace Drupal\editorial_group\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsButtonsWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\editorial_group\EditorialGroupInterface;
use Drupal\editorial_group\EditorialGroupServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation for editorial group entity reference widget.
 *
 * Base on the regular options widget (checkboxes). Unselectable groups are
 * hidden, that is, groups not assigned to the acting user.
 *
 * @FieldWidget(
 *   id = "editorial_group",
 *   label = @Translation("Editorial group"),
 *   field_types = {
 *     "entity_reference",
 *   },
 *   multiple_values = TRUE
 * )
 */
class EditorialGroupWidget extends OptionsButtonsWidget {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The editorial group service.
   *
   * @var \Drupal\editorial_group\EditorialGroupServiceInterface
   */
  protected $editorialGroupService;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, AccountInterface $current_user, EditorialGroupServiceInterface $editorial_group_service) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->currentUser = $current_user;
    $this->editorialGroupService = $editorial_group_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('editorial_group')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return parent::isApplicable($field_definition)
      && $field_definition->getName() == EditorialGroupInterface::ACCESS_FIELD_NAME
      && $field_definition->getFieldStorageDefinition()->getProvider() == 'editorial_group'
      && $field_definition->getTargetEntityTypeId() != 'user';
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $form_element = parent::formElement($items, $delta, $element, $form, $form_state);

    if ($this->currentUser->hasPermission('administer editorial groups')) {
      // Administrators has no restrictions.
      return $form_element;
    }

    $form_object = $form_state->getFormObject();
    if (!$form_object instanceof ContentEntityFormInterface) {
      // Not in a content entity form.
      return $form_element;
    }

    // If entity belongs to editorial groups that the current user is not
    // registered, field is required to avoid loss of entity access.
    $entity = $form_object->getEntity();
    $entity_groups = $this->editorialGroupService->getAccessGroups($entity);
    $form_element['#required'] = !empty($entity_groups)
      && $entity_groups != $this->editorialGroupService->getAccessGroups($form_object->getEntity(), TRUE);

    $user_groups = $this->editorialGroupService->getGrantedGroups();
    foreach (array_keys($form_element['#options']) as $option_key) {
      if (!isset($user_groups[$option_key])) {
        // User cannot manage groups to which it does not belong.
        $form_element[$option_key]['#access'] = FALSE;
      }
    }

    return $form_element;
  }

}
