<?php

namespace Drupal\editorial_group\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Ensure own group constraint.
 *
 * @Constraint(
 *   id = "EditorialGroupOwnGroup",
 *   label = @Translation("Own group", context = "Validation"),
 * )
 */
class OwnGroupConstraint extends Constraint {

  /**
   * Error message.
   *
   * @var string
   */
  public $errorMessage = 'At least one of your assigned groups must be selected, otherwise you will loose editorial access to this content.';

}
