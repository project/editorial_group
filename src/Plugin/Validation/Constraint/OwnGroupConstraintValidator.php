<?php

namespace Drupal\editorial_group\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\editorial_group\EditorialGroupServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the own group constraint.
 */
class OwnGroupConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The editorial group service.
   *
   * @var \Drupal\editorial_group\EditorialGroupServiceInterface
   */
  protected $editorialGroupService;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Creates a new own group validator instance.
   *
   * @param \Drupal\editorial_group\EditorialGroupServiceInterface $editorial_group_service
   *   The editorial group service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(EditorialGroupServiceInterface $editorial_group_service, AccountInterface $current_user) {
    $this->editorialGroupService = $editorial_group_service;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('editorial_group'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    if (!$items->count() || $this->currentUser->hasPermission('administer editorial groups')) {
      return;
    }

    $user_groups = $this->editorialGroupService->getGrantedGroups();
    foreach ($items as $delta => $item) {
      if (isset($user_groups[$item->target_id])) {
        // At least one user group found.
        return;
      }
    }

    $this->context->buildViolation($constraint->errorMessage)
      ->atPath($delta)
      ->addViolation();
  }

}
