<?php

namespace Drupal\editorial_group;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining an editorial group entity type.
 */
interface EditorialGroupInterface extends ContentEntityInterface {

  /**
   * Editorial group reference access field name.
   */
  public const ACCESS_FIELD_NAME = 'editorial_group';

  /**
   * Granted editorial group reference field name.
   */
  public const GRANT_FIELD_NAME = 'editorial_group_grant';

}
