<?php

namespace Drupal\editorial_group;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Editorial group service.
 */
interface EditorialGroupServiceInterface {

  /**
   * Gets the IDs of the entity types under editorial group access control.
   *
   * @return array
   *   Array of the enabled entity type IDs.
   */
  public function getEnabledTypes();

  /**
   * Gets the editorial groups of a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to be checked.
   * @param bool $filtered
   *   (optional) Limit results to groups also granted to a given user
   *   account. Not filtering by default.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   (optional) Account for result filtering. Current user by default.
   *
   * @return null|array
   *   Array of group labels indexed by their IDs. Empty array if no groups
   *   enabled for the given entity o there was no matches with the user
   *   granted groups. NULL if the entity type is not under editorial group
   *   access control.
   */
  public function getAccessGroups(EntityInterface $entity, bool $filtered = FALSE, AccountInterface $account = NULL);

  /**
   * Gets the editorial groups that a given user belongs to.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   (optional) The account. Current user by default.
   *
   * @return array
   *   Array of group labels indexed by their IDs. Empty array if the user has
   *   no groups.
   */
  public function getGrantedGroups(AccountInterface $account = NULL);

  /**
   * Checks if editorial group access control can be applied on a given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to be checked.
   *
   * @return bool
   *   TRUE if the entity's type is editorial group enabled. FALSE otherwise.
   */
  public function isEnabledEntity(EntityInterface $entity);

  /**
   * Checks if editorial group access control is active on an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to be checked.
   *
   * @return bool
   *   TRUE if the entity is editorial group enabled and grouped in, at least,
   *   one editorial group. FALSE otherwise.
   */
  public function isControlledEntity(EntityInterface $entity);

  /**
   * Updates entity definitions according to changes on enabled types.
   */
  public function updateEntityDefinitions();

  /**
   * Check access to a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   (optional) The user account. Current user by default.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Access result, forbidden if access denied by editorial group, neutral
   *   otherwise.
   */
  public function checkAccess(EntityInterface $entity, AccountInterface $account = NULL);

}
