<?php

namespace Drupal\editorial_group\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\editorial_group\EditorialGroupServiceInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Editorial Group event subscriber.
 */
class EditorialGroupSubscriber implements EventSubscriberInterface {

  /**
   * The editorial_group service.
   *
   * @var \Drupal\editorial_group\EditorialGroupServiceInterface
   */
  protected $editorialGroup;

  /**
   * Constructs an EditorialGroupSubscriber object.
   *
   * @param \Drupal\editorial_group\EditorialGroupServiceInterface $editorial_group
   *   The editorial_group service.
   */
  public function __construct(EditorialGroupServiceInterface $editorial_group) {
    $this->editorialGroup = $editorial_group;
  }

  /**
   * Dispatches any change on the editorial group settings.
   */
  public function onConfigEvent(ConfigCrudEvent $event) {
    $config = $event->getConfig();
    if ($config->getName() != 'editorial_group.settings') {
      return;
    }

    $this->editorialGroup->updateEntityDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ConfigEvents::SAVE => ['onConfigEvent'],
      ConfigEvents::DELETE => ['onConfigEvent'],
    ];
  }

}
