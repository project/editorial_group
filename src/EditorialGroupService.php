<?php

namespace Drupal\editorial_group;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;

/**
 * Service description.
 */
class EditorialGroupService implements EditorialGroupServiceInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Entity Definition Update Manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected $entityDefinitionUpdateManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The Editorial Group settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $configuration;

  /**
   * Constructs an EditorialGroupService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $entity_definition_update_manager
   *   The entity definition update manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityDefinitionUpdateManagerInterface $entity_definition_update_manager, ConfigFactoryInterface $config_factory, AccountInterface $current_user) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDefinitionUpdateManager = $entity_definition_update_manager;
    $this->configuration = $config_factory->get('editorial_group.settings');
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function getEnabledTypes() {
    return $this->configuration->get('enabled_entity_types') ?: [];
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessGroups(EntityInterface $entity, bool $filtered = FALSE, AccountInterface $account = NULL) {
    if (!$this->isEnabledEntity($entity)) {
      return NULL;
    }

    $group_ids = $this->getEntityGroupIds($entity);
    if (empty($group_ids)) {
      return [];
    }

    if ($filtered) {
      $group_ids = array_intersect($group_ids, $this->getEntityGroupIds($this->getUserEntity($account), 'grant'));
      if (empty($group_ids)) {
        return [];
      }
    }

    return $this->loadGroups($group_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getGrantedGroups(AccountInterface $account = NULL) {
    return $this->loadGroups($this->getEntityGroupIds($this->getUserEntity($account), 'grant'));
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabledEntity(EntityInterface $entity) {
    return $entity instanceof ContentEntityInterface
      && array_search($entity->getEntityTypeId(), $this->getEnabledTypes()) !== FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isControlledEntity(EntityInterface $entity) {
    // If entity is enabled, it is a content entity.
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    return $this->isEnabledEntity($entity)
      && !$entity->get(EditorialGroupInterface::ACCESS_FIELD_NAME)->isEmpty();
  }

  /**
   * Loads group label for a given array of IDs.
   *
   * @param array $group_ids
   *   Array of IDs.
   *
   * @return array
   *   Array of group labels indexed by their IDs. Given references to
   *   non-existing groups are filtered.
   */
  protected function loadGroups(array $group_ids) {
    $groups = [];
    foreach ($this->entityTypeManager->getStorage('editorial_group')->loadMultiple($group_ids) as $editorial_group) {
      $groups[$editorial_group->id()] = $editorial_group->label();
    }

    return $groups;
  }

  /**
   * Gets the user entity for a given account.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   (optional) The account. Current user by default.
   */
  protected function getUserEntity(AccountInterface $account = NULL) {
    if (!$account) {
      $account = $this->currentUser;
    }

    return $account instanceof UserInterface
      ? $account
      : $this->entityTypeManager->getStorage('user')->load($account->id());
  }

  /**
   * Gets editorial group IDs of a given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity. Must be an editorial group enabled entity,
   *   @see \Drupal\editorial_group\EditorialGroupServiceInterface::isEnabledEntity()
   * @param string $type
   *   (optional) Type of the source field. One of 'access' (default) or
   *   'grant'.
   *
   * @return null|array
   *   Array of group IDs. Empty array if the field has no values.
   */
  protected function getEntityGroupIds(ContentEntityInterface $entity, $type = 'access') {
    $groups_field = $type == 'grant'
      ? $entity->get(EditorialGroupInterface::GRANT_FIELD_NAME)
      : $entity->get(EditorialGroupInterface::ACCESS_FIELD_NAME);
    $group_ids = [];
    foreach ($groups_field->getValue() as $field_value) {
      $group_ids[] = $field_value['target_id'];
    }

    return $group_ids;
  }

  /**
   * Builds entity group base field definition.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $type
   *   Type of field. One of:
   *   - 'access': (default) access control field.
   *   - 'grant': granted groups, suitable only for the user entity.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   *   Base field definition.
   */
  public static function groupReferenceFieldDefinition(string $entity_type_id, $type = 'access') {
    // Common field parameteres.
    $field_definition = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Editorial groups'))
      ->setSetting('target_type', 'editorial_group')
      ->setSetting('handler_settings', [
        'sort' => [
          'field' => 'name',
          'order' => 'ASC',
        ],
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setProvider('editorial_group');

    if ($type == 'grant') {
      $field_definition
        ->setDescription(t('Grants the user access to edit contents of the selected groups.'))
        ->setDisplayOptions('form', [
          'type' => 'options_buttons',
          'weight' => -5,
        ]);
    }
    else {
      $field_definition
        ->setDescription(t('Only users from the selected groups can edit this content.'))
        ->addConstraint('EditorialGroupOwnGroup')
        ->setDisplayOptions('form', [
          'type' => 'editorial_group',
          'weight' => 5,
        ])
        ->setDisplayConfigurable('form', TRUE);
    }

    return $field_definition;
  }

  /**
   * {@inheritdoc}
   */
  public function updateEntityDefinitions() {
    $todo = [
      'create' => [],
      'remove' => [],
    ];

    foreach ($this->entityDefinitionUpdateManager->getChangeList() as $entity_type_id => $change) {
      if (!isset($change['field_storage_definitions'][EditorialGroupInterface::ACCESS_FIELD_NAME])) {
        continue;
      }

      switch ($change['field_storage_definitions'][EditorialGroupInterface::ACCESS_FIELD_NAME]) {
        case EntityDefinitionUpdateManagerInterface::DEFINITION_CREATED:
          $todo['create'][] = $entity_type_id;
          break;

        case EntityDefinitionUpdateManagerInterface::DEFINITION_DELETED:
          $todo['remove'][] = $entity_type_id;
          break;
      }
    }

    if (!empty($todo['create']) || !empty($todo['remove'])) {
      $this->entityTypeManager->clearCachedDefinitions();
    }

    foreach ($todo['create'] as $entity_type_id) {
      $this->entityDefinitionUpdateManager->installFieldStorageDefinition(EditorialGroupInterface::ACCESS_FIELD_NAME, $entity_type_id, 'editorial_group', static::groupReferenceFieldDefinition($entity_type_id));
    }

    foreach ($todo['remove'] as $entity_type_id) {
      $storage_definition = $this->entityDefinitionUpdateManager->getFieldStorageDefinition(EditorialGroupInterface::ACCESS_FIELD_NAME, $entity_type_id);
      $this->entityDefinitionUpdateManager->uninstallFieldStorageDefinition($storage_definition);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkAccess(EntityInterface $entity, ?AccountInterface $account = NULL) {
    if ($account->hasPermission('administer editorial groups')) {
      return AccessResult::neutral('No restrictions to editorial group administrators.');
    }

    if (!$this->isControlledEntity($entity)) {
      return AccessResult::neutral('Entity is not under editorial group access control.');
    }

    return empty($this->getAccessGroups($entity, TRUE, $account))
      ? AccessResult::forbidden('No editorial groups matched.')
      : AccessResult::allowed('Editorial group match.');
  }

  /**
   * Filter editorial groups on entity presave to non-administrator users.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  public function entityPresave(EntityInterface $entity) {
    if (!$this->isEnabledEntity($entity) || $this->currentUser->hasPermission('administer editorial groups')) {
      return;
    }

    // Only content entity types can be editorial group enabled.
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $allowed = array_keys($this->getAccessGroups($entity, TRUE));
    if (isset($entity->original) && $this->isControlledEntity($entity->original)) {
      $allowed = array_merge($allowed, array_diff($this->getEntityGroupIds($entity->original), array_keys($this->getGrantedGroups())));
    }

    $entity->editorial_group = $allowed;
  }

}
