<?php

namespace Drupal\editorial_group\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\editorial_group\EditorialGroupInterface;

/**
 * Defines the editorial group entity class.
 *
 * @ContentEntityType(
 *   id = "editorial_group",
 *   label = @Translation("Editorial group"),
 *   label_collection = @Translation("Editorial groups"),
 *   label_singular = @Translation("editorial group"),
 *   label_plural = @Translation("editorial groups"),
 *   label_count = @PluralTranslation(
 *     singular = "@count editorial groups",
 *     plural = "@count editorial groups",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\editorial_group\EditorialGroupListBuilder",
 *     "access" = "Drupal\editorial_group\EditorialGroupAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\editorial_group\Form\EditorialGroupForm",
 *       "edit" = "Drupal\editorial_group\Form\EditorialGroupForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "editorial_group",
 *   data_table = "editorial_group_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer editorial groups",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/people/editorial-group",
 *     "add-form" = "/admin/people/editorial-group/add",
 *     "edit-form" = "/admin/people/editorial-group/{editorial_group}",
 *     "delete-form" = "/admin/people/editorial-group/{editorial_group}/delete",
 *   },
 * )
 */
class EditorialGroup extends ContentEntityBase implements EditorialGroupInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setTranslatable(TRUE)
      ->setLabel(t('Name'))
      ->setDescription(t('The editorial group name. It must be unique.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->addConstraint('UniqueField', [])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ]);

    return $fields;
  }

}
