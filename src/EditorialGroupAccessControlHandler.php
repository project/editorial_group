<?php

namespace Drupal\editorial_group;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the access control handler for the "Editorial Group" entity type.
 *
 * @see \Drupal\editorial_group\Entity\EditorialGroup
 */
class EditorialGroupAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  /**
   * {@inheritdoc}
   */
  protected $viewLabelOperation = TRUE;

  /**
   * The editorial group service.
   *
   * @var \Drupal\editorial_group\EditorialGroupServiceInterface
   */
  protected $editorialGroupService;

  /**
   * Constructs the editorial group access control handler instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\editorial_group\EditorialGroupServiceInterface $editorial_group_service
   *   The editorial group service.
   */
  public function __construct(EntityTypeInterface $entity_type, EditorialGroupServiceInterface $editorial_group_service) {
    parent::__construct($entity_type);
    $this->editorialGroupService = $editorial_group_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('editorial_group')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($operation == 'view label') {
      return AccessResult::allowedIf($account->hasPermission('administer editorial groups') || $this->editorialGroupService->getGrantedGroups($account));
    }
    else {
      return parent::checkAccess($entity, $operation, $account);
    }
  }

}
