<?php

/**
 * @file
 * Primary module hooks for Editorial Group module.
 */

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\editorial_group\EditorialGroupInterface;
use Drupal\editorial_group\EditorialGroupService;

/**
 * Implements hook_entity_base_field_info().
 */
function editorial_group_entity_base_field_info(EntityTypeInterface $entity_type) {
  $fields = [];
  if ($entity_type->id() == 'user') {
    // Add the granted editorial groups field to the user entity.
    $fields[EditorialGroupInterface::GRANT_FIELD_NAME] = EditorialGroupService::groupReferenceFieldDefinition($entity_type->id(), 'grant');
  }

  $enabled_types = \Drupal::service('editorial_group')->getEnabledTypes();
  if (array_search($entity_type->id(), $enabled_types) !== FALSE) {
    $fields[EditorialGroupInterface::ACCESS_FIELD_NAME] = EditorialGroupService::groupReferenceFieldDefinition($entity_type->id());
  }

  return $fields;
}

/**
 * Implements hook_entity_access().
 */
function editorial_group_entity_access(EntityInterface $entity, $operation, AccountInterface $account) {
  if ($operation != 'update' && $operation != 'delete') {
    return AccessResult::neutral('Editorial group access control only affects update or delete operations.');
  }

  return \Drupal::service('editorial_group')->checkAccess($entity, $account);
}

/**
 * Implements hook_entity_presave().
 */
function editorial_group_entity_presave(EntityInterface $entity) {
  \Drupal::service('editorial_group')->entityPresave($entity);
}

/**
 * Implements hook_entity_field_access().
 */
function editorial_group_entity_field_access($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
  switch ($field_definition->getName()) {
    case EditorialGroupInterface::ACCESS_FIELD_NAME:
      // User cannot set editorial group access restrictions if has not
      // been granted previously with someone.
      $result = AccessResult::forbiddenIf(
        !$account->hasPermission('administer editorial groups')
        && empty(\Drupal::service('editorial_group')->getGrantedGroups($account)
      ), 'User account has not granted any editorial group.');
      break;

    case EditorialGroupInterface::GRANT_FIELD_NAME:
      // Only editorial group administrators are allowed to grant groups to
      // users.
      $result = AccessResult::forbiddenIf(!$account->hasPermission('administer editorial groups'), 'Editorial group administrators access always granted.');
      break;

    default:
      $result = AccessResult::neutral('Not a editorial group field.');
      break;
  }

  return $result;
}
